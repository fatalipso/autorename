package fr.fatalispo;

import java.io.*;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public final static String COMMA_DELIMITER = ",";
    public static final String BASE_PATH = "D:\\Fatalispo_projects\\";
    static boolean readingFile = false;

    public static void main(String[] args) throws IOException {
        listenForNewFile();
    }

    public static LinkedList<Product> readCSVFile(String fileName) throws IOException {
        LinkedList<Product> products = new LinkedList<>();
        File file = new File(BASE_PATH + fileName);
        String rowOfDataFromCSV = "";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            while ((rowOfDataFromCSV = br.readLine()) != null) {
                String[] productDetails = rowOfDataFromCSV.split(COMMA_DELIMITER);
                List<String> list = new ArrayList<>(Arrays.asList(productDetails));
                list.removeAll(Collections.singleton(""));
                productDetails = list.toArray(new String[0]);

                if (productDetails.length > 0) {
                    String object = productDetails[0];
                    String reason = productDetails[1];
                    int lineID = Integer.parseInt(productDetails[2]);
                    String date = productDetails[3];
                    int ref = Integer.parseInt(productDetails[4]);
                    String productName = productDetails[5];
                    String otherDate = productDetails[6];
                    String actionType = productDetails[7];
                    double cashLess = Double.parseDouble(productDetails[8]);
                    double cashMore = Double.parseDouble(productDetails[9]);

                    //Save the products details in Product object
                    Product prod = new Product(object, reason, lineID, date, ref, productName, otherDate, actionType, cashLess, cashMore);
                    products.add(prod);
                }
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
        return products;
    }

    private static boolean sumValuesForSimilarProducts(LinkedList<Product> products, String fileName, String productLabel, int productNumber) throws IOException {
        File file = new File(BASE_PATH + fileName);
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter fileOutputWriter = null;
        boolean canDoNextFile;


        //Extract from CSV and put in LinkedList
        try {
            Iterator<Product> iterator = products.iterator();
            Double cashIn = 0d, cashOut = 0d;
            while (iterator.hasNext()) {
                Product product = iterator.next();
                if (product.getRef() == productNumber) {
                    cashIn += product.getCashMore();
                    cashOut += product.getCashLess();
                    iterator.remove();
                }
            }

            //Default product for products to change
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String date = sdf.format(Calendar.getInstance().getTime()), actionType = "Encaissement du " + date;
            if (products.size() > 0) {
                date = products.get(0).getDate();
                actionType = products.get(0).getActionType();
            }
            Product productDefault = new Product("CA", "CAISSE", 1, date, productNumber, productLabel,
                    date, actionType, 0d, 0d);

            //if there is a cash out, create a product for this
            if (cashIn != 0) {
                productDefault.setCashLess(0d);
                productDefault.setCashMore(cashIn);
                products.addFirst(productDefault);
            }
            if (cashOut != 0) {
                productDefault.setCashMore(0d);
                productDefault.setCashLess(cashOut);
                products.set(cashIn != 0 ? 1 : 0, productDefault);
            }

            fileOutputStream = new FileOutputStream(file);
            fileOutputWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            int idNewProd = 0;
            for (Product product : products) {
                idNewProd++;
                fileOutputWriter.append(product.getObject() + COMMA_DELIMITER + product.getReason() + COMMA_DELIMITER + idNewProd + COMMA_DELIMITER + product.getDate() +
                        COMMA_DELIMITER + product.getRef() + COMMA_DELIMITER + product.getProductName() + COMMA_DELIMITER + product.getOtherDate() + COMMA_DELIMITER + product.getActionType() +
                        COMMA_DELIMITER + product.getCashLess() + COMMA_DELIMITER + product.getCashMore() + "\n");
            }
        } catch (Exception ee) {
            ee.printStackTrace();
            if (fileOutputWriter != null) {
                fileOutputWriter.flush();
                fileOutputWriter.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } finally {
            if (fileOutputWriter != null) {
                fileOutputWriter.flush();
                fileOutputWriter.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            canDoNextFile = false;
        }
        return canDoNextFile;
    }

    private static void listenForNewFile() throws IOException {
        //folder to watch
        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path dir = new File(BASE_PATH).toPath();
        dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        ArrayList<String> fileListToProcess = new ArrayList<>();
        ReentrantLock reentrantLock = new ReentrantLock();

        //loop as long as the folder exist
        while (true) {
            WatchKey key = null;
            try {
                key = watchService.take();
                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind<?> kind = event.kind();

                    if (kind != StandardWatchEventKinds.ENTRY_CREATE) {
                        continue;
                    }
                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    String fileEvent = ev.context().toString();
                    if (kind == StandardWatchEventKinds.ENTRY_CREATE && fileEvent != null && fileEvent.startsWith("Export")) {
                        if (!fileListToProcess.contains(fileEvent)) {
                            fileListToProcess.add(fileEvent);
                        }
                        try {
                            if (reentrantLock.tryLock(10, TimeUnit.SECONDS)) {
                                final ArrayList<String> finalFileListToProcess = new ArrayList<>(fileListToProcess);
                                Iterator<String> iterator = finalFileListToProcess.iterator();
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            while (!finalFileListToProcess.isEmpty()) {
                                                readingFile = false;
                                                if (iterator.hasNext() && !readingFile) {
                                                    String finalFileEvent = iterator.next();
                                                    readingFile = true;
                                                    readingFile = sumValuesForSimilarProducts(readCSVFile(finalFileEvent), finalFileEvent, "TABAC.LOGISTA", 4671);
                                                    iterator.remove();
                                                }
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }).start();
                            }

                        } finally {
                            reentrantLock.unlock();
                        }
                    }
                }
            } catch (InterruptedException ex) {
                return;
            } finally {
                key.reset();

            }
        }
    }
}

