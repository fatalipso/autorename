package fr.fatalispo;

public class Product {
    private String object;
    private String reason;
    private int lineID;
    private String date;
    private int ref;
    private String productName;
    private String otherDate;
    private String actionType;
    private double cashLess;
    private double cashMore;

    public Product() {}

    public Product(String object, String reason, int lineID, String date, int ref, String productName, String otherDate, String actionType, double cashLess, double cashMore) {
        this.object = object;
        this.reason = reason;
        this.lineID = lineID;
        this.date = date;
        this.ref = ref;
        this.productName = productName;
        this.otherDate = otherDate;
        this.actionType = actionType;
        this.cashLess = cashLess;
        this.cashMore = cashMore;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getLineID() {
        return lineID;
    }

    public void setLineID(int lineID) {
        this.lineID = lineID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRef() {
        return ref;
    }

    public void setRef(int ref) {
        this.ref = ref;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOtherDate() {
        return otherDate;
    }

    public void setOtherDate(String otherDate) {
        this.otherDate = otherDate;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public double getCashLess() {
        return cashLess;
    }

    public void setCashLess(double cashLess) {
        this.cashLess = cashLess;
    }

    public double getCashMore() {
        return cashMore;
    }

    public void setCashMore(double cashMore) {
        this.cashMore = cashMore;
    }
}
